package com.example.lukaszcisek.chartsample;

import android.support.annotation.NonNull;

import com.example.lukaszcisek.chartsample.model.User;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Luke.
 */

public final class DataManager {

    private DataManager() {
    }

    @NonNull
    public static List<User> getUserrDate() {
        return Arrays.asList(User.create(1, "Aaa", "a1a1a1", 10),
                User.create(2, "Bbb", "b1b1b1", 10),
                User.create(3, "Ccc", "c1c1c1", 12),
                User.create(4, "Ddd", "d1d1d1", 10),
                User.create(5, "Eee", "e1e1e1", 10),
                User.create(6, "Fff", "f1f1f1", 12),
                User.create(7, "Ggg", "g1g1g1", 10),
                User.create(8, "Hhh", "h1h1h1", 10),
                User.create(9, "Iii", "i1i1i1", 13),
                User.create(10, "Jjj", "j1j1j1", 10),
                User.create(11, "Kkk", "k1k1k1", 10),
                User.create(12, "Lll", "l1l1l1", 12),
                User.create(13, "Mmm", "m1m1m1", 10),
                User.create(14, "Nnn", "n1n1n1", 11),
                User.create(15, "Sss", "s1s1s1", 10)
        );
    }
}

package com.example.lukaszcisek.chartsample.model;

import com.google.auto.value.AutoValue;

/**
 * Created by Luke.
 */
@AutoValue
public abstract class User {

    public abstract int id();

    public abstract String firstName();

    public abstract String lastName();

    public abstract int age();

    public static User create(int id, String firstName, String lastName, int age) {
        return new AutoValue_User(id, firstName, lastName, age);
    }
}
